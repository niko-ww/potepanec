class Potepan::CategoriesController < ApplicationController
  def show
    @taxonomies = Spree::Taxonomy.includes(:taxons)
    @taxon = Spree::Taxon.find(params[:id])
    @color_values = Spree::OptionType.find_by(presentation: "Color").option_values
    @size_values = Spree::OptionType.find_by(presentation: "Size").option_values
    @products = @taxon.all_products.includes(master: [:default_price, :images]).order(available_on: :desc)

    if params[:value] && params[:sort]
      @products = @taxon.all_products.includes(master: [:default_price, :images]).
        filter_by_value(params[:value]).
        sort_out_by(params[:sort])
    elsif params[:value]
      @products = @taxon.all_products.includes(master: [:default_price, :images]).
        filter_by_value(params[:value]).order(available_on: :desc)
    elsif params[:sort]
      @products = @taxon.all_products.includes(master: [:default_price, :images]).
        sort_out_by(params[:sort])
    end
  end
end
