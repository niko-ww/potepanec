class Potepan::LineItemsController < ApplicationController
  def destroy
    line_item = Spree::LineItem.find_by(id: params[:id])
    line_item.order.contents.remove_line_item(line_item)
    redirect_to potepan_cart_path
  end
end
