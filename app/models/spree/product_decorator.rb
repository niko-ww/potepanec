module Spree::ProductDecorator
  def related_products(related_product_max_num)
    Spree::Product.in_taxons(taxons).
      includes(master: [:images, :default_price]).
      where.not(id: id).distinct.limit(related_product_max_num)
  end

  def self.prepended(base)
    base.scope :desc_order_available, -> do
      base.includes(master: [:images, :default_price]).
        order(available_on: :desc)
    end

    base.scope :filter_by_value, -> (value) do
      return nil if value.blank?
      base.includes(variants: :option_values).
        where(spree_option_values: { presentation: value })
    end

    base.scope :sort_out_by, -> (sort) do
      return nil if sort.blank?
      if sort == "new"
        base.order(available_on: :desc)
      elsif sort == "low_price"
        base.ascend_by_master_price
      elsif sort == "high_price"
        base.descend_by_master_price
      elsif sort == "old"
        base.order(available_on: :asc)
      end
    end
  end

  Spree::Product.prepend self
end
