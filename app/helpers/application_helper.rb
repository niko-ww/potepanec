module ApplicationHelper
  def full_title(page_title)
    base_title = "BIGBAG Store"
    if page_title.blank?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  def option_products_count(value:, category:)
    Spree::Product.includes(variants: :option_values).
      in_taxon(category).
      where(spree_option_values: { presentation: value.presentation }).count
  end
end
