class Potepan::OrderMailer < ApplicationMailer
  def confirm_email(order)
    @order = order
    @ship_address = order.ship_address
    @store = order.store

    subject = "注文完了！！ 注文番号[#{order.number}]"
    mail to: order.email, from: "#{@store.name}<#{@store.mail_from_address}>", subject: subject
  end
end
