# Preview all emails at http://localhost:3000/rails/mailers/potepan/order
class Potepan::OrderPreview < ActionMailer::Preview
  # => http://localhost:3000/rails/mailers/spree/mailer_previews/order
  # Preview this email at http://localhost:3000/rails/mailers/potepan/order/order_complete
  def confirm_email
    order = Spree::Order.first
    Potepan::OrderMailer.confirm_email(order)
  end
end
