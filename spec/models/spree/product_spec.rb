require "rails_helper"

RSpec.describe Spree::Product, type: :model do
  describe "scope" do
    describe "desc_order_available" do
      subject { Spree::Product.desc_order_available }

      let!(:new_product) { create(:product, available_on: Time.current) }
      let!(:middle) { create_list(:product, 4, available_on: 1.week.ago) }
      let!(:old_product) { create(:product, available_on: 1.month.ago) }

      it "new_productが最新の商品である" do
        expect(subject.first).to eq(new_product)
      end

      it "old_productが最後の商品である" do
        expect(subject.last).to eq(old_product)
      end
    end

    describe "filter_by_value" do
      subject { Spree::Product.filter_by_value(value) }

      let!(:product) { create(:product) }
      let!(:red_product)   { create(:product, variants: [color_variant]) }
      let!(:color_variant) { create(:variant, option_values: [color_value]) }
      let!(:color_value)   { create(:option_value, presentation: "Red") }
      let!(:s_product)    { create(:product, variants: [size_variant]) }
      let!(:size_variant) { create(:variant, option_values: [size_value]) }
      let!(:size_value)   { create(:option_value, presentation: "S") }

      context "valueがnilの時" do
        let(:value) { nil }

        it "全ての商品を返す" do
          is_expected.to include(product, red_product, s_product)
        end
      end

      context "valueがRedの時" do
        let(:value) { "Red" }

        it "Redの商品のみを返す" do
          is_expected.to eq([red_product])
        end
      end

      context "valueがsの時" do
        let!(:value) { "S" }

        it "Sサイズの商品のみを返す" do
          is_expected.to eq([s_product])
        end
      end
    end

    describe "sort_out_by" do
      subject { Spree::Product.sort_out_by(sort) }

      let!(:new_product) { create(:product, available_on: Time.current) }
      let!(:old_product) { create(:product, available_on: 1.year.ago) }
      let!(:high_product) { create(:product, price: "30") }
      let!(:low_product) { create(:product, price: "5") }

      context "sortが_new_の時" do
        let(:sort) { "new" }

        it "新着順に商品を返す" do
          expect(subject.first).to eq(new_product)
          expect(subject.last).to eq(old_product)
        end
      end

      context "sortが_old_の時" do
        let(:sort) { "old" }

        it "古い順に商品を返す" do
          expect(subject.first).to eq(old_product)
          expect(subject.last).to eq(new_product)
        end
      end

      context "sortが_high_price_の時" do
        let(:sort) { "high_price" }

        it "価格の高い順に商品を返す" do
          expect(subject.first).to eq(high_product)
          expect(subject.last).to eq(low_product)
        end
      end

      context "sortが_low_price_の時" do
        let(:sort) { "low_price" }

        it "価格の安い順に商品を返す" do
          expect(subject.first).to eq(low_product)
          expect(subject.last).to eq(high_product)
        end
      end
    end
  end
end
