require "rails_helper"

RSpec.describe Spree::Product, type: :model do
  let(:order) { create(:order, state: state) }

  describe "previous_state" do
    subject { order.previous_state }

    context "state = 'address' の時" do
      let(:state) { "address" }

      it { is_expected.to eq "cart" }
    end

    context "state = 'peyment' の時" do
      let(:state) { "payment" }

      it { is_expected.to eq "address" }
    end

    context "state = 'confirm'" do
      let(:state) { "confirm" }

      it { is_expected.to eq "payment" }
    end
  end

  describe "step_design" do
    subject { order.step_design }

    context "state = 'address' の時" do
      let(:state) { "address" }

      it { is_expected.to eq "disabled" }
    end

    context "state = 'peyment' の時" do
      let(:state) { "payment" }

      it { is_expected.to eq "active" }
    end

    context "state = 'confirm' の時" do
      let(:state) { "confirm" }

      it { is_expected.to eq "complete" }
    end
  end

  describe "payment_by(methods)" do
    let(:state) { "address" }
    let!(:credit_card) do
      create(:payment_method, available_to_users: true, available_to_admin: true)
    end

    it "credit_cardを返す" do
      expect(order.payment_by("Credit Card")).to eq(credit_card)
    end
  end
end
