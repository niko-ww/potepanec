require 'rails_helper'

RSpec.describe CheckoutHelper, type: :helper do
  describe "#section_title(order)" do
    subject { section_title(order) }

    let(:order) { create(:order, state: state) }

    context "order.state = 'address' の時" do
      let(:state) { "address" }

      it { is_expected.to eq("お届け先情報") }
    end

    context "order.state = 'payment' の時" do
      let(:state) { "payment" }

      it { is_expected.to eq("お支払い方法") }
    end

    context "order.state = 'confirm' の時" do
      let(:state) { "confirm" }

      it { is_expected.to eq("入力内容確認") }
    end
  end
end
