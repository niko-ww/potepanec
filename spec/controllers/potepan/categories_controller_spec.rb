require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon)    { create(:taxon, taxonomy: taxonomy, products: products) }
    let!(:products)       { [*create_list(:product, 3), color_product, size_product] }
    let!(:other_products) { create(:product) }
    let!(:color_product) { create(:product, variants: [color_variant]) }
    let!(:color_variant) { create(:variant, option_values: [color_value]) }
    let!(:color_value)   { create(:option_value, presentation: "Red", option_type: color_type) }
    let!(:color_type)    { create(:option_type, presentation: "Color") }
    let!(:size_product) { create(:product, variants: [size_variant]) }
    let!(:size_variant) { create(:variant, option_values: [size_value]) }
    let!(:size_value)   { create(:option_value, presentation: "S", option_type: size_type) }
    let!(:size_type)    { create(:option_type, presentation: "Size") }

    before { get :show, params: { id: taxon.id } }

    describe "response" do
      subject { response }

      it "レスポンスが正常である" do
        is_expected.to be_successful
        is_expected.to render_template(:show)
      end
    end

    describe "インスタンス変数" do
      it '@taxonomyが期待する値を持つ' do
        expect(assigns(:taxonomies)).to eq(Spree::Taxonomy.all)
      end

      it '@taxonが期待する値を持つ' do
        expect(assigns(:taxon)).to eq(taxon)
      end

      it "@color_valuesが期待する値を持つ" do
        expect(assigns(:color_values)).to eq([color_value])
      end

      it "@size_valuesが期待する値を持つ" do
        expect(assigns(:size_values)).to eq([size_value])
      end
    end

    describe "@products" do
      context "商品カテゴリーから選択した場合" do
        it '@productsが期待する値を持つ' do
          expect(assigns(:products)).to match_array(products)
        end

        it '@productsが期待しない値を持たない' do
          expect(assigns(:products)).not_to include(other_products)
        end
      end

      context "-色から探す-から選択した場合" do
        before { get :show, params: { id: taxon.id, value: color_value.presentation } }

        it "@productsが期待する値を持つ" do
          expect(assigns(:products)).to eq [color_product]
        end
      end

      context "-サイズから探す-から選択した場合" do
        before { get :show, params: { id: taxon.id, value: size_value.presentation } }

        it "@productsが期待する値を持つ" do
          expect(assigns(:products)).to eq [size_product]
        end
      end

      context "ソートする場合" do
        let!(:new_product) { create(:product, available_on: Time.current, taxons: [taxon]) }
        let!(:old_product) { create(:product, available_on: 2.year.ago, taxons: [taxon]) }
        let!(:high_product) { create(:product, price: "30.00", taxons: [taxon]) }
        let!(:low_product) { create(:product, price: "1.00", taxons: [taxon]) }

        context "新着順に並び替えた時" do
          before { get :show, params: { id: taxon.id, sort: "new" } }

          it "新着順に商品を返す" do
            expect(assigns(:products).first).to eq(new_product)
            expect(assigns(:products).last).to eq(old_product)
          end
        end

        context "高い順に並び替えた時" do
          before { get :show, params: { id: taxon.id, sort: "high_price" } }

          it "高い順に商品を返す" do
            expect(assigns(:products).first).to eq(high_product)
            expect(assigns(:products).last).to eq(low_product)
          end
        end

        context "Redの商品を新着順に並び替えた時" do
          let!(:color_new) { create(:variant, product: new_product, option_values: [color_value]) }
          let!(:color_old) { create(:variant, product: old_product, option_values: [color_value]) }

          before { get :show, params: { id: taxon.id, value: color_value.presentation, sort: "new" } }

          it "新着順にRedの商品を返す" do
            expect(assigns(:products).first).to eq(new_product)
            expect(assigns(:products).last).to eq(old_product)
          end
        end

        context "Sサイズの商品を高い順に並び替えた時" do
          let!(:size_high) { create(:variant, product: high_product, option_values: [size_value]) }
          let!(:size_low)  { create(:variant, product: low_product, option_values: [size_value]) }

          before { get :show, params: { id: taxon.id, value: size_value.presentation, sort: "high_price" } }

          it "高い順にSサイズの商品を返す" do
            expect(assigns(:products).first).to eq(high_product)
            expect(assigns(:products).last).to eq(low_product)
          end
        end
      end
    end
  end
end
