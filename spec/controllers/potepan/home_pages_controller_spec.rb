require 'rails_helper'

RSpec.describe Potepan::HomePagesController, type: :controller do
  describe "index" do
    before { get :index }

    it "正常にレスポンスを返す" do
      expect(response).to be_successful
    end

    it ":indexテンプレートが表示される" do
      expect(response).to render_template(:index)
    end

    describe "new_release_function" do
      let!(:new_release_products) { create_list(:product, 10, available_on: 1.month.ago) }
      let!(:old_product) { create(:product, available_on: 1.year.ago) }
      let!(:latest_product) { create(:product, available_on: Time.current) }
      let!(:second_latest) { create(:product, available_on: 1.day.ago) }

      it "@new_release_productsが期待されない値を含まない" do
        expect(assigns(:new_release_products)).not_to include(old_product)
      end

      it "@new_release_productsが８件までの値を持つ" do
        expect(assigns(:new_release_products).size).to eq(8)
      end

      it "新着商品の表示順がavailable_onの降順であること" do
        expect(assigns(:new_release_products).first).to eq(latest_product)
        expect(assigns(:new_release_products).second).to eq(second_latest)
      end
    end
  end
end
