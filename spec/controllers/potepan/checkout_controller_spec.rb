require 'rails_helper'

RSpec.describe Potepan::CheckoutController, type: :controller do
  subject { response }

  let(:token)  { "token" }
  let!(:user)  { create(:user) }
  let!(:order) { create(:order_with_totals, user: user, guest_token: token) }

  before do
    allow(controller).to receive_messages(try_spree_current_user: user)
    allow(controller).to receive_messages(current_order: order)
    cookies.signed[:guest_token] = token
  end

  describe "#edit" do
    describe "レスポンス" do
      before { get :edit, params: { state: "address" } }

      it { is_expected.to be_successful }
      it { is_expected.to render_template(:edit) }
    end

    describe "before_action" do
      let(:state) { "address" }
      let(:edit_address) { get :edit, params: { state: state } }

      it "load_orderが正常な動作である" do
        allow(controller).to receive_messages(current_order: nil)
        edit_address
        is_expected.to redirect_to(potepan_cart_path)
      end

      it "set_state_if_presentが正常な動作である" do
        allow(order).to receive_messages(can_go_to_state?: true)
        edit_address
        expect(assigns(:order).state).to eq state
      end

      it "ensure_checkout_allowedが正常な動作である" do
        allow(order).to receive_messages(checkout_allowed?: false)
        edit_address
        is_expected.to redirect_to(potepan_cart_path)
      end

      it "ensure_order_not_completeが正常な動作である" do
        allow(order).to receive_messages(completed?: true)
        edit_address
        is_expected.to redirect_to(potepan_cart_path)
      end

      it "ensure_sufficient_stock_linesが正常な動作である" do
        allow(order).
          to receive_message_chain("insufficient_stock_lines.present?").
          and_return(true)
        allow(order).
          to receive_message_chain("insufficient_stock_lines.collect.to_sentence").
          and_return("item")

        edit_address
        is_expected.to redirect_to potepan_cart_path
      end

      it "ensure_valid_stateが正常な動作である" do
        allow(order).to receive_messages(has_checkout_step?: false)
        edit_address
        is_expected.to redirect_to potepan_checkout_state_path(order.checkout_steps.first)
      end
    end
  end

  describe "#update" do
    describe "before_action" do
      let(:state) { "address" }
      let(:update_address) { patch :update, params: { state: state } }

      it "load_orderが正常である" do
        update_address
        expect(assigns(:order)).to eq order
      end

      it "set_state_if_presentが正常な動作である" do
        allow(order).to receive_messages can_go_to_state?: true
        update_address
        expect(assigns(:order).state).to eq state
      end

      it "ensure_checkout_allowedが正常な動作である" do
        allow(order).to receive_messages checkout_allowed?: false
        update_address
        is_expected.to redirect_to potepan_cart_path
      end

      it "ensure_order_not_completeが正常な動作である" do
        allow(order).to receive_messages completed?: true
        update_address
        is_expected.to redirect_to potepan_cart_path
      end

      it "ensure_sufficient_stock_linesが正常な動作である" do
        allow(order).
          to receive_message_chain("insufficient_stock_lines.present?").
          and_return(true)
        allow(order).
          to receive_message_chain("insufficient_stock_lines.collect.to_sentence").
          and_return("item")

        update_address
        is_expected.to redirect_to(potepan_cart_path)
      end

      it "ensure_valid_stateが正常な動作である" do
        allow(order).to receive_messages has_checkout_step?: false
        update_address
        is_expected.to redirect_to potepan_checkout_state_path(order.checkout_steps.first)
      end
    end

    describe "orderの更新" do
      context "params[:state]が’address’の時" do
        let(:ship_address) do
          address = build(:address)
          address.attributes.except("created_at", "updated_at")
        end

        let(:update_address) do
          patch :update, params: {
            state: "address",
            order: {
              ship_address_attributes: ship_address,
              use_billing: true,
            },
          }
        end

        let!(:order) { create(:order_with_line_items, guest_token: token, state: "address") }

        it "paramsを送信すると、orderのstateが’address’から’payment’に切り替わる" do
          expect { update_address }.to change(order, :state).from("address").to("payment")
        end
      end

      context "params[:state]が’payment’の時" do
        let(:order)           { create(:order_with_line_items, guest_token: token) }
        let!(:payment_method) { create(:credit_card_payment_method) }

        let(:update_payment) do
          patch :update, params: {
            state: "payment", order: {
              payments_attributes: [{
                payment_method_id: payment_method.id,
                source_attributes: attributes_for(:credit_card),
              }],
            },
          }
        end

        before { 3.times { order.next! } }

        context 'with a permitted payment method' do
          it 'sets the payment amount' do
            expect { update_payment }.to change(order, :state).from("payment").to("confirm")
            expect(order.payments.size).to eq 1
            is_expected.to redirect_to potepan_checkout_state_path("confirm")
          end
        end
      end

      context "params[:state]が’confirm'の時" do
        let!(:order) { create(:order_ready_to_complete, guest_token: token) }
        let(:update_confirm) { patch :update, params: { state: "confirm" } }

        it "paramsを送信すると、orderのstateが’confirm’から’complete’に切り替わる" do
          expect { update_confirm }.to change(order, :state).from("confirm").to("complete")
        end

        it "注文完了ページに遷移する" do
          update_confirm
          is_expected.to redirect_to potepan_order_path(order.number)
        end
      end
    end
  end
end
