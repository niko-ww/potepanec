require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  subject { response }

  describe '#show' do
    let(:taxon1) { create(:taxon) }
    let(:taxon2) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon1]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon1]) }
    let(:not_related_product) { create(:product, taxons: [taxon2]) }

    before { get :show, params: { id: product.id } }

    describe "レスポンス" do
      it '正常にレスポンスを返す' do
        is_expected.to be_successful
        is_expected.to render_template(:show)
      end
    end

    describe "@products" do
      it '@productが期待される値を持つ' do
        expect(assigns(:product)).to eq(product)
      end
    end

    describe "@related_products" do
      it '@related_productsが４件までの値を持つ' do
        expect(assigns(:related_products).size).to eq(4)
      end

      it '@related_productsが関連商品ではない値を含まない' do
        expect(assigns(:related_products)).not_to include(not_related_product)
      end

      it '@related_productsが関連元の商品を含まない' do
        expect(assigns(:related_products)).not_to include(product)
      end
    end
  end

  describe "#search" do
    let(:search_product) { create(:product, name: "test123", description: "this is test") }
    let(:other_search_product) { create(:product, name: "sample", description: "this is sample") }

    before { get :search, params: { search: "test123" } }

    describe "レスポンス" do
      it "正常なレスポンスを返す" do
        is_expected.to be_successful
        is_expected.to render_template(:search)
      end
    end

    describe "@search_keyword" do
      it "@search_keywordが期待する値を持つ" do
        expect(assigns(:search_keyword)).to eq("test123")
      end
    end

    describe "@search_product" do
      context "検索対象が商品名の時" do
        it "@search_productが期待する値を持つ" do
          expect(assigns(:search_products)).to include(search_product)
        end

        it "@search_productが期待しない値を持たない" do
          expect(assigns(:search_products)).not_to include(other_search_product)
        end
      end

      context "検索対象が商品の説明文の時" do
        let(:word) { "this" }

        before { get :search, params: { search: word } }

        it "@search_productが期待する値を持つ" do
          expect(assigns(:search_products)).to include(search_product, other_search_product)
        end
      end

      context "検索対象が商品名で後方一致する時" do
        let(:word) { "123" }

        before { get :search, params: { search: word } }

        it "@search_productが期待する値を持つ" do
          expect(assigns(:search_products)).to include(search_product)
        end

        it "@search_productsが期待しない値を持たない" do
          expect(assigns(:search_products)).not_to include(other_search_product)
        end
      end
    end
  end
end
