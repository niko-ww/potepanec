require 'rails_helper'

RSpec.feature "SearchPages", type: :feature do
  subject { page }

  let!(:search_product) { create(:product, name: "test123", description: "this is nice") }
  let!(:other_search_product) { create(:product, name: "sample", description: "this is sample") }

  before { visit potepan_root_path }

  scenario "ユーザーが商品を検索する" do
    fill_in "search", with: "test"
    click_on "検索"

    expect(current_path).to eq(search_potepan_products_path)
    within(".productBox") do
      is_expected.to have_link search_product.name, href: potepan_product_path(search_product.id)
      is_expected.to have_no_content(other_search_product.name)
    end
  end
end
