require 'rails_helper'

RSpec.feature "Orders", type: :feature do
  include ApplicationHelper
  subject { page }

  let!(:user)    { create(:user) }
  let!(:taxon)   { create(:taxon, products: [product]) }
  let!(:product) { create(:product) }
  let!(:store)   { create(:store) }
  let!(:state)   { create(:state) }
  let(:order)      { user.orders.last }
  let(:line_item)  { user.orders.last.line_items.last }
  let(:line_items) { user.orders.last.line_items }

  before do
    allow_any_instance_of(Potepan::OrdersController).to receive_messages(try_spree_current_user: user)
    allow_any_instance_of(Potepan::CheckoutController).to receive_messages(try_spree_current_user: user)
  end

  scenario "カートが空の状態の、カートページを表示する" do
    visit potepan_cart_path

    is_expected.to have_content "カートは空です"
    is_expected.to have_no_link "次へ"
  end

  scenario "カートへ入れるをクリックする" do
    add_products_to_cart

    is_expected.to have_title full_title("Cart")
    expect(current_path).to eq potepan_cart_path

    within ".table" do
      is_expected.to have_link nil, href: potepan_line_item_path(line_item.id)
      is_expected.to have_link product.name
      is_expected.to have_content product.display_price
      is_expected.to have_field "order_line_items_attributes_0_quantity", with: 2
      is_expected.to have_content "$#{2 * product.price.to_f}"
      line_items.each do |line_item|
        line_item.display_amount
      end
    end

    within ".totalAmountArea" do
      is_expected.to have_content order.display_item_total
      is_expected.to have_content order.display_tax_total
      is_expected.to have_content order.display_total
    end
  end

  scenario "line_itemの削除ボタンをクリックすると、削除される" do
    add_products_to_cart

    is_expected.to have_link product.name

    find("#delete_button").click

    expect(current_path).to eq potepan_cart_path
    is_expected.to have_no_link product.name
    is_expected.to have_content "カートは空です"
  end

  scenario "アップデートボタンをクリックすると、注文内容が変わる" do
    add_products_to_cart
    fill_in "order_line_items_attributes_0_quantity", with: 3
    click_button "アップデート"

    expect(current_path).to eq potepan_cart_path
    is_expected.to have_field "order_line_items_attributes_0_quantity", with: 3
    within(".line-item-amount") do
      is_expected.to have_content "$#{3 * product.price.to_f}"
    end
  end

  scenario "’購入する’をクリックすると、お届け先情報のページに遷移する" do
    add_products_to_cart
    click_on "購入する"

    expect(current_path).to eq potepan_checkout_state_path("address")
  end

  def add_products_to_cart
    visit potepan_product_path(product.id)
    select "2", from: "quantity"
    click_button "カートへ入れる"
  end
end
