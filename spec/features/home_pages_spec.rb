require 'rails_helper'

RSpec.feature "Home_pages", type: :feature do
  subject { page }

  let!(:taxon) { create(:taxon) }
  let!(:latest_product) { create(:product, name: "new", taxons: [taxon], available_on: Time.current) }
  let!(:new_products) { create_list(:product, 7, name: "new", available_on: 1.day.ago) }
  let!(:old_product) { create(:product, price: "10", available_on: 1.year.ago) }

  before { visit potepan_root_path }

  scenario "新着商品が表示される" do
    within(".featuredProductsSlider") do
      is_expected.to have_link "new", count: 8
      is_expected.to have_content(latest_product.display_price)
      is_expected.to have_content(new_products[0].display_price)
      is_expected.not_to have_link(old_product.name)
      is_expected.not_to have_content(old_product.display_price)
    end
  end

  scenario "ユーザーが新着商品をクリックすると、詳細ページに移動する" do
    click_on latest_product.name, match: :first

    expect(current_path).to eq(potepan_product_path(latest_product.id))
    within("#media_body") do
      is_expected.to have_content(latest_product.name)
      is_expected.to have_content(latest_product.display_price)
      is_expected.to have_content(latest_product.description)
    end
  end
end
